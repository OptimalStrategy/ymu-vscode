import * as vscode from 'vscode';
import * as request from 'request-promise';
import { ConfigObject, StatusResponse, TrackStatus, LikedResponse, VolumeResponse, HistoryResponse, QualityResponse } from './data';
import { truncateTitle, getLikedStatus, getPlayModeEmoji, _logError } from './utils';

class YmuUI {
    _skipStatusBarItem: vscode.StatusBarItem | null;
    _backStatusBarItem: vscode.StatusBarItem | null;
    _volumeStatusBarItem: vscode.StatusBarItem | null;
    _trackStatusBarItem: vscode.StatusBarItem | null;
    _likeStatusBarItem: vscode.StatusBarItem | null;
    _copyStatusBarItem: vscode.StatusBarItem | null;

    constructor() {
        this._skipStatusBarItem = null;
        this._backStatusBarItem = null;
        this._volumeStatusBarItem = null;
        this._trackStatusBarItem = null;
        this._likeStatusBarItem = null;
        this._copyStatusBarItem = null;
    }

    get trackStatusBarItem(): vscode.StatusBarItem {
        return this._trackStatusBarItem != null ? this._trackStatusBarItem : this._ui_error();
    }

    set trackStatusBarItem(item: vscode.StatusBarItem) {
        this._trackStatusBarItem = item;
    }

    get likeStatusBarItem(): vscode.StatusBarItem {
        return this._likeStatusBarItem != null ? this._likeStatusBarItem : this._ui_error();
    }

    set likeStatusBarItem(item: vscode.StatusBarItem) {
        this._likeStatusBarItem = item;
    }

    get skipStatusBarItem(): vscode.StatusBarItem {
        return this._skipStatusBarItem != null ? this._skipStatusBarItem : this._ui_error();
    }

    set skipStatusBarItem(item: vscode.StatusBarItem) {
        this._skipStatusBarItem = item;
    }

    get backStatusBarItem(): vscode.StatusBarItem {
        return this._backStatusBarItem != null ? this._backStatusBarItem : this._ui_error();
    }

    set backStatusBarItem(item: vscode.StatusBarItem) {
        this._backStatusBarItem = item;
    }

    get copyStatusBarItem(): vscode.StatusBarItem {
        return this._copyStatusBarItem != null ? this._copyStatusBarItem : this._ui_error();
    }

    set copyStatusBarItem(item: vscode.StatusBarItem) {
        this._copyStatusBarItem = item;
    }


    get volumeStatusBarItem(): vscode.StatusBarItem {
        return this._volumeStatusBarItem != null ? this._volumeStatusBarItem : this._ui_error();
    }

    set volumeStatusBarItem(item: vscode.StatusBarItem) {
        this._volumeStatusBarItem = item;
    }

    checkInitialized() {
        if (this._trackStatusBarItem == null
            || this._likeStatusBarItem == null
            || this._skipStatusBarItem == null
            || this._copyStatusBarItem == null
            || this._backStatusBarItem == null
            || this._volumeStatusBarItem == null
        ) {
            this._ui_error();
        }
    }

    forEach(callback: (item: vscode.StatusBarItem) => void) {
        const buttons = [
            this.skipStatusBarItem,
            this.backStatusBarItem,
            this.volumeStatusBarItem,
            this.trackStatusBarItem,
            this.likeStatusBarItem,
            this.copyStatusBarItem,
        ];

        buttons.forEach(callback);
    }

    hideAll() {
        this.forEach((i) => i.hide())
    }

    showAll() {
        this.forEach((i) => i.show())
    }

    _ui_error(): vscode.StatusBarItem {
        throw new Error('UI is not configured.');
    }
}

class _TrackStatusState {
    default_title: string | null = null;
    previous_label: string | null = null;
    last_status: TrackStatus | null = null;
}

class APIController {
    _config: ConfigObject | null;
    _pollingInterval: number;
    _logPolling: boolean = false;
    _maxTitleLength: number;
    _pollerIsRunning: boolean = false;
    _ssState: _TrackStatusState;
    ui: YmuUI;

    constructor(ui: YmuUI, pollingInterval: number = 300, maxTitleLength: number = 48) {
        this.ui = ui;
        this._config = null;
        this._pollingInterval = pollingInterval;
        this._maxTitleLength = maxTitleLength;
        this._ssState = new _TrackStatusState();
    }

    get url(): string {
        return `http://${this.config.server.host}:${this.config.server.port}`;
    }

    get config(): ConfigObject {
        if (this._config === null) {
            throw new Error("Config is null.");
        }

        return this._config;
    }

    set config(conf: ConfigObject) {
        this._config = conf;
    }

    async get(path: string, handler: (...args: any[]) => void, error: (...args: any[]) => void): Promise<any> {
        try {
            return request.get(`${this.url}/${path}`)
                .then(handler)
                .catch(error);
        } catch (e) {
            error(e);
        }
    }

    async sendStatus(callback: (r: StatusResponse) => void, error: (...args: any[]) => void): Promise<any> {
        return this.get("status", r => {
            let sr: StatusResponse = JSON.parse(r);
            callback(sr);
        }, error);
    }

    async sendPlayOrPause(): Promise<any> {
        return this.sendStatus(async sr => {
            await this.get(sr.data.playing ? "pause" : "play", _ => { }, _logError);
        }, _logError);
    }

    async sendLike(): Promise<any> {
        this.ui.likeStatusBarItem.text = "...";

        return this.get("like", r => {
            let status: LikedResponse = JSON.parse(r);
            let prevStatus = this._ssState.last_status;
            if (prevStatus != null) {
                prevStatus.favorite = status.data.favorite;
                this.setState(prevStatus);
            }
        }, _logError);
    }

    async _sendPlayItem(endpoint: string, query: string, callback: (sr: StatusResponse) => void, error: (...args: any[]) => void): Promise<any> {
        return this.get(
            `${endpoint}?${query}`,
            (r) => {
                let status: StatusResponse = JSON.parse(r);
                callback(status)
            },
            error,
        );
    }

    async sendPlayTrack(track: string, callback: (sr: StatusResponse) => void, error: (...args: any[]) => void): Promise<any> {
        return this._sendPlayItem(
            "play_track",
            `track=${encodeURIComponent(track)}`,
            callback,
            error
        );
    }

    async sendPlayAlbumByLink(link: string, callback: (sr: StatusResponse) => void, error: (...args: any[]) => void): Promise<any> {
        return this._sendPlayItem(
            "play_album",
            `link=${encodeURIComponent(link)}`,
            callback,
            error
        );
    }

    async sendPlayAlbumById(id: number, callback: (sr: StatusResponse) => void, error: (...args: any[]) => void): Promise<any> {
        return this._sendPlayItem(
            "play_album",
            `id=${encodeURIComponent(id)}`,
            callback,
            error
        );
    }

    async sendPlayRadio(index: number, callback: (sr: StatusResponse) => void, error: (...args: any[]) => void): Promise<any> {
        return this._sendPlayItem(
            "play_radio",
            `index=${encodeURIComponent(index)}`,
            callback,
            error
        );
    }

    async sendPlayPlaylist(url: string, callback: (sr: StatusResponse) => void, error: (...args: any[]) => void): Promise<any> {
        return this._sendPlayItem(
            "play_playlist",
            `playlist=${encodeURIComponent(url)}`,
            callback,
            error
        );
    }

    async sendTrackHistory(callback: (hr: HistoryResponse) => void, error: (...args: any[]) => void): Promise<any> {
        return this.get(
            "song_history",
            (r) => {
                let history: HistoryResponse = JSON.parse(r);
                callback(history);
            },
            error
        );
    }

    async sendSkip(): Promise<any> {
        return this.get("skip", _ => { }, _logError);
    }

    async sendBack(): Promise<any> {
        return this.get("back", _ => { }, _logError);
    }

    async sendVolume(volume: number): Promise<any> {
        this.ui.volumeStatusBarItem.text = "🔊 ...";
        return this.get(`volume?value=${encodeURIComponent(volume)}`, r => {
            let volume: VolumeResponse = JSON.parse(r);
            this.ui.volumeStatusBarItem.text = `🔊 ${Math.round(volume.data.volume)}%`;
        }, _logError);
    }

    async sendHqOn(): Promise<any> {
        return this.get('hq_on', r => {
            let q: QualityResponse = JSON.parse(r);
            if (q.data) {
                vscode.window.showInformationMessage("Switched to HQ");
            }
        }, _logError);
    }

    async sendHqOff(): Promise<any> {
        return this.get('hq_on', r => {
            let q: QualityResponse = JSON.parse(r);
            if (q.data) {
                vscode.window.showInformationMessage("Switched to LQ");
            }
        }, _logError);
    }

    async sendShutdown(): Promise<any> {
        return this.get("shutdown", () => { }, _logError);
    }

    async withState(callback: (r: TrackStatus) => void) {
        if (this._ssState.last_status == null) {
            await this.sendStatus((ss) => callback(ss.data), _logError);
        } else {
            callback(this._ssState.last_status);
        }
    }

    _startPoller() {
        vscode.window.showInformationMessage(`Successfully connected to the ymusic server at ${this.url}.`);

        let instance = this;
        let lastPoll = Date.now();

        setTimeout(async function _poll() {
            instance._pollerIsRunning = true;

            if (instance._logPolling) {
                console.log(`Time since last poll: ${Date.now() - lastPoll}ms.`);
                lastPoll = Date.now();
            }

            await instance.sendStatus(sr => {
                instance.setState(sr.data);
                setTimeout(_poll, instance._pollingInterval);
            }, e => {
                console.error(e);
                vscode.window.showErrorMessage("Failed to connect to the server. Attempting to reconnect...");

                instance.setDummy();
                instance._pollerIsRunning = false;
                instance.listen_for_heartbeats();
            })
        }, this._pollingInterval)
    }

    setState(status: TrackStatus) {
        let song = truncateTitle(
            `${status.title} - ${status.artist}`, this._maxTitleLength
        );
        let playing = status.playing;
        let progress = Math.round(status.progress);
        let liked = getLikedStatus(status.favorite);
        let mode = getPlayModeEmoji(status.kind);
        let label = `${mode} | ${playing ? '🎶' : '⏸'} ${song} [${progress}%]`;
        let volume = `🔊 ${Math.round(status.volume)}%`;

        if (this._ssState.default_title != null) {
            this._ssState.default_title = status.default_title;
        }

        if (status.kind.endsWith("RADIO")) {
            this.ui.backStatusBarItem.hide();
        } else {
            this.ui.backStatusBarItem.show();
        }

        if (this._ssState.previous_label != label && status.title != this._ssState.default_title) {
            this._ssState.last_status = status;
            this._ssState.previous_label = label;
            this.ui.trackStatusBarItem.text = label;
            this.ui.likeStatusBarItem.text = liked;
            this.ui.volumeStatusBarItem.text = volume;
        }
    }

    setDummy() {
        this._ssState.last_status = null;
        this._ssState.previous_label = null;
        this.ui.likeStatusBarItem.text = "...";
        this.ui.volumeStatusBarItem.text = "🔊 ...";
        this.ui.trackStatusBarItem.text = "Loading ...";
    }

    showErrorIfServerIsDown(): boolean {
        if (!this._pollerIsRunning) {
            vscode.window.showErrorMessage("The server appears to be down.");
            return true;
        }

        return false;
    }

    listen_for_heartbeats(delay: number = 250, attempts_to_show_error_message: number = 1000, _attempts: number = 0) {
        setTimeout(async () => {
            await this.get("__heartbeat__", (r) => {
                if (r == "OK") {
                    this._startPoller();
                    return;
                } else {
                    this.listen_for_heartbeats(delay, attempts_to_show_error_message, _attempts);
                }
            }, (e) => {
                console.log(`Listening for heartbeats: ${_attempts} / ${attempts_to_show_error_message}`);

                ++_attempts;
                if (_attempts >= attempts_to_show_error_message) {
                    _attempts = 0;
                    vscode.window.showErrorMessage(
                        `Could not connect to the server at ${this.url} in ${attempts_to_show_error_message} attempts:\n${e}.`
                    );
                }

                this.listen_for_heartbeats(delay, attempts_to_show_error_message, _attempts);
            })
        }, delay)
    }

    get modeChangeSuccess() {
        return (sr: StatusResponse) => {
            this.setState(sr.data);
            vscode.window.showInformationMessage(`Playing ${sr.data.title} - ${sr.data.artist}.`);
        }
    }

    modeChangeError(noun: string, item: any) {
        return (e: any) => {
            vscode.window.showErrorMessage(`Could not play the ${noun}: \`${item}\`:\n${e}`)
        };
    }
}

export { APIController, YmuUI }
