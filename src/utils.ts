import * as vscode from 'vscode';

function truncateTitle(title: string, max_title_length: number = 50): string {
    if (title.length > max_title_length) {
        title = title.substring(0, max_title_length);
    }
    return title;
}

function getLikedStatus(enumStr: string) {
    return enumStr == "LikeResult.REMOVED" ? "Disliked 💔" : "Liked ❤";
}

function getPlayModeEmoji(enumStr: string) {
    switch (enumStr) {
        case "ContentKind.RADIO": return "📻";
        case "ContentKind.ALBUM": return "💿";
        case "ContentKind.PLAYLIST": return "🖼";
        default: return "🤔";
    }
}

function _logError(e: any) {
    console.log(e);
}

function registerCommand(subs: { dispose(): any }[], id: string, callback: (...args: any[]) => void): string {
    subs.push(vscode.commands.registerCommand(id, () => setTimeout(callback, 0)));
    return id;
}

function createStatus(
    subs: { dispose(): any }[],
    commandId: string,
    text: string,
    priority: number
) {
    let item = vscode.window.createStatusBarItem(vscode.StatusBarAlignment.Right, priority);
    item.text = text;
    item.command = commandId;
    item.show();
    subs.push(item);
    return item;
}

function generateHistoryHtml(history: Array<string>): string {
    let historyList = "<ol>";

    history.forEach(item => {
        historyList += `<li>${item}</li>\n`;
    })

    historyList += "</ol>";

    return `<!DOCTYPE html>
      <html lang="en">
      <head>
          <meta charset="UTF-8">
          <meta name="viewport" content="width=device-width, initial-scale=1.0">
          <title>Ymu Track History</title>
      </head>
      <body>
        ${historyList}
      </body>
      </html>`;
}

export {
    truncateTitle,
    getLikedStatus,
    registerCommand,
    createStatus,
    _logError,
    getPlayModeEmoji,
    generateHistoryHtml
}
