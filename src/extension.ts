import * as vscode from 'vscode';
import * as fs from 'fs';

import { CONFIG, PositionDelta } from './constants';
import { APIController as YmuAPI, YmuUI } from './api';
import { registerCommand, createStatus, generateHistoryHtml } from './utils';

let UI: YmuUI = new YmuUI();
let API: YmuAPI = new YmuAPI(UI, 150);

export function activate({ subscriptions }: vscode.ExtensionContext) {
	try {
		API.config = JSON.parse(fs.readFileSync(CONFIG, 'utf8'));
		console.log(`Successfully loaded the player config: \`${API.config}\`.`);
	} catch (e) {
		vscode.window.showErrorMessage(`Could not open the config at \`${CONFIG}\`: ${e}. Run the server first to generate the default config.`);
		return console.log(`Failed to open the config at \`${CONFIG}\`.`);
	}

	let dt = new PositionDelta();

	const skipStatusBarItemCommandId = registerCommand(subscriptions, 'ymu.skipCurrentTrack', async () => {
		if (API.showErrorIfServerIsDown()) {
			return;
		}

		await API.sendSkip()
	});

	UI.skipStatusBarItem = createStatus(
		subscriptions,
		skipStatusBarItemCommandId,
		"Skip ⏭",
		dt.next()
	);

	const backStatusBarItemCommandId = registerCommand(subscriptions, 'ymu.skipBack', async () => {
		if (API.showErrorIfServerIsDown()) {
			return;
		}


		API.withState(async (ss) => {
			if (ss.kind.endsWith("RADIO")) {
				vscode.window.showErrorMessage("Radio stations do not support skipping back.")
			} else {
				await API.sendBack();
			}
		})

	});

	UI.backStatusBarItem = createStatus(
		subscriptions,
		backStatusBarItemCommandId,
		"⏮ Back",
		dt.next()
	);

	const volumeStatusBarCommandId = registerCommand(subscriptions, 'ymu.setVolume', async () => {
		if (API.showErrorIfServerIsDown()) {
			return;
		}

		let options: vscode.InputBoxOptions = {
			prompt: "Enter the desired volume value (volume is logarithmic)",
			placeHolder: "e.g. 100",
		}

		vscode.window.showInputBox(options).then(async value => {
			if (!value) return;

			let volume = parseInt(value, 10);

			if (Number.isNaN(volume)) {
				return vscode.window.showErrorMessage(`Could not update the volume: invalid number.`);
			} else if (volume > 100) {
				return vscode.window.showErrorMessage(`The maximum volume value is 100, got ${volume}.`);
			} else if (volume < 0) {
				return vscode.window.showErrorMessage(`The minimum volume value is 0, got ${volume}.`);
			}

			await API.sendVolume(volume);
		});
	});

	UI.volumeStatusBarItem = createStatus(
		subscriptions,
		volumeStatusBarCommandId,
		"🔊 ...",
		dt.next()
	);

	const trackStatusBarCommandId = registerCommand(subscriptions, 'ymu.pauseOrPlayCurrentTrack', async () => {
		if (API.showErrorIfServerIsDown()) {
			return;
		}

		await API.sendPlayOrPause()
	});

	UI.trackStatusBarItem = createStatus(
		subscriptions,
		trackStatusBarCommandId,
		"... Loading",
		dt.next()
	);

	const likeStatusBarCommandId = registerCommand(subscriptions, 'ymu.likeOrDislikeCurrentTrack', async () => {
		if (API.showErrorIfServerIsDown()) {
			return;
		}

		await API.sendLike()
	});

	UI.likeStatusBarItem = createStatus(
		subscriptions,
		likeStatusBarCommandId,
		"...",
		dt.next()
	);

	const copyTrackUrlStatusBarCommandId = registerCommand(subscriptions, 'ymu.copyCurrentTrack', async () => {
		if (API.showErrorIfServerIsDown()) {
			return;
		}

		API.withState((ss) => {
			vscode.env.clipboard.writeText(ss.song_url)
		})
	});

	UI.copyStatusBarItem = createStatus(
		subscriptions,
		copyTrackUrlStatusBarCommandId,
		"📋 Track",
		dt.next()
	);

	registerCommand(subscriptions, 'ymu.SetPollingInterval', () => {
		let options: vscode.InputBoxOptions = {
			prompt: "Enter the number of milliseconds",
			placeHolder: "e.g. 300"
		}

		vscode.window.showInputBox(options).then(value => {
			if (!value) return;

			let ms = parseInt(value, 10);

			if (Number.isNaN(ms)) {
				return vscode.window.showErrorMessage(`Could not update the polling interval: invalid number.`);
			} else if (ms < 100) {
				return vscode.window.showErrorMessage(`The minimum polling interval is 100, got ${ms}.`);
			}

			API._pollingInterval = ms;
		});
	});

	registerCommand(subscriptions, 'ymu.TogglePollingLogging', () => {
		API._logPolling = !API._logPolling;
	});

	registerCommand(subscriptions, 'ymu.launch', () => { });

	registerCommand(subscriptions, 'ymu.playTrack', () => {
		if (API.showErrorIfServerIsDown()) {
			return;
		}

		let options: vscode.InputBoxOptions = {
			prompt: "Paste a track URL",
			placeHolder: "e.g. https://music.yandex.com/album/1234567/track/1234567"
		}

		vscode.window.showInputBox(options).then(async value => {
			if (!value) return;

			await API.sendPlayTrack(
				value,
				API.modeChangeSuccess,
				API.modeChangeError("track", value),
			)
		});
	});

	registerCommand(subscriptions, 'ymu.playAlbum', () => {
		if (API.showErrorIfServerIsDown()) {
			return;
		}

		let options: vscode.InputBoxOptions = {
			prompt: "Paste an album URL or ID",
			placeHolder: "e.g. 1234567 or https://music.yandex.com/album/1234567/"
		}

		vscode.window.showInputBox(options).then(async value => {
			if (!value) return;

			let album = parseInt(value, 10);

			if (Number.isNaN(album)) {
				await API.sendPlayAlbumByLink(
					value,
					API.modeChangeSuccess,
					API.modeChangeError("album", album)
				)
			} else if (album > 0) {
				await API.sendPlayAlbumById(
					album,
					API.modeChangeSuccess,
					API.modeChangeError("album", value)
				)
			} else {
				vscode.window.showErrorMessage(`Album id must be greater than 0, got ${album}.`);
			}
		});
	});

	registerCommand(subscriptions, 'ymu.playRadio', () => {
		if (API.showErrorIfServerIsDown()) {
			return;
		}

		let options: vscode.InputBoxOptions = {
			prompt: "Enter a radio index or leave empty to play the default radio",
			placeHolder: "e.g. 1"
		}

		vscode.window.showInputBox(options).then(async value => {
			let index = parseInt(value || "1", 10);

			if (Number.isNaN(index)) {
				vscode.window.showErrorMessage(`Radio index must be a number, got ${index}.`);
			} else if (index <= 0) {
				vscode.window.showErrorMessage(`Radio index must be greater than 0, got ${index}.`)
			} else {
				await API.sendPlayRadio(
					index,
					API.modeChangeSuccess,
					API.modeChangeError("radio", index)
				)
			}
		});
	});

	registerCommand(subscriptions, 'ymu.playPlaylist', () => {
		if (API.showErrorIfServerIsDown()) {
			return;
		}

		let options: vscode.InputBoxOptions = {
			prompt: "Enter a playlist URL",
			placeHolder: "e.g. https://music.yandex.com/users/yamusic-daily/playlists/12345678"
		}

		vscode.window.showInputBox(options).then(async value => {
			if (!value) return;

			await API.sendPlayPlaylist(
				value,
				API.modeChangeSuccess,
				API.modeChangeError("playlist", value)
			)
		});
	});

	registerCommand(subscriptions, 'ymu.displayHistory', async () => {
		if (API.showErrorIfServerIsDown()) {
			return;
		}

		await API.sendTrackHistory((hr) => {
			const panel = vscode.window.createWebviewPanel(
				'ymu:trackHistory',
				'Ymu Track History',
				vscode.ViewColumn.One,
				{}
			);
			panel.webview.html = generateHistoryHtml(hr.data.history);
		}, (e) => {
			vscode.window.showErrorMessage(`Could not retrieve the track history:\n${e}`);
		})
	});

	registerCommand(subscriptions, "ymu.shutdownServer", async () => {
		if (API.showErrorIfServerIsDown()) {
			return;
		}

		await API.sendShutdown();
	})

	registerCommand(subscriptions, "ymu.hqOn", async () => {
		if (API.showErrorIfServerIsDown()) {
			return;
		}

		await API.sendHqOn();
	})

	registerCommand(subscriptions, "ymu.hqOff", async () => {
		if (API.showErrorIfServerIsDown()) {
			return;
		}

		await API.sendHqOff();
	})

	registerCommand(subscriptions, 'ymu.regroupUI', () => {
		UI.hideAll();
		UI.showAll();
	});

	UI.checkInitialized();
	API.listen_for_heartbeats();
}
