export interface YmuResponse<D> {
    data: D,
    status: string,
}

export interface SongHistory {
    dump: boolean,
    path: string,
}

export interface AuthSettings {
    login: string | null,
    remote_auth: boolean,
}

export interface ServerSettings {
    host: string,
    port: number,
}

export interface ConfigObject {
    use_cookies: boolean,
    save_cookies: boolean,
    notifications: boolean,
    always_open_captcha_window: boolean,
    icon: string,
    driver: string,
    default_volume: number,
    auth: AuthSettings,
    server: ServerSettings,
    history: SongHistory,
}

export interface TrackStatus {
    albumArt: string,
    artist: string,
    default_title: string | null,
    favorite: string,
    kind: string,
    playing: boolean,
    progress: number,
    quality: string,
    song_url: string,
    station: string | null,
    title: string,
    volume: number,
}

export interface LikedStatus {
    favorite: string,
}

export interface VolumeStatus {
    volume: number,
}

export interface ErrorStatus {
    error: string,
    error_meta: string | null
}

export interface HistoryStatus {
    history: Array<string>,
}

export type QualityStatus = boolean;


export type StatusResponse = YmuResponse<TrackStatus>;
export type LikedResponse = YmuResponse<LikedStatus>;
export type ErrorResponse = YmuResponse<ErrorStatus>;
export type VolumeResponse = YmuResponse<VolumeStatus>;
export type HistoryResponse = YmuResponse<HistoryStatus>;
export type QualityResponse = YmuResponse<QualityStatus>;