import * as path from 'path';
import * as os from 'os';

const homedir: string = os.homedir();
const CONFIG_DIR: string = path.join(homedir, ".yplayer");
const CONFIG_NAME: string = "yplayer.json";
const CONFIG: string = path.join(CONFIG_DIR, CONFIG_NAME);

const PRIORITY: number = 1042;
const PRIORITY_DELTA: number = 0.000000000001;


class PositionDelta {
    current: number;

    constructor() {
        this.current = 0;
    }

    next(): number {
        return pdelta(this.current++);
    }
}

function pdelta(n: number = 0): number {
    return PRIORITY + PRIORITY_DELTA * n;
}

export {
    CONFIG_DIR,
    CONFIG_NAME,
    CONFIG,
    PRIORITY,
    PRIORITY_DELTA,
    PositionDelta,
}