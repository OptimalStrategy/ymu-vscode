# Ymu-server Status Bar Extension


This extension allows to control [ymu-server](https://gitlab.com/OptimalStrategy/ymusic-player/) via status bar buttons.

![](https://i.imgur.com/eXSqSj6.gif)

---

## Commands
| Command                       | Description                                                                   | Implemented |
| ----------------------------- | ----------------------------------------------------------------------------- | ----------: |
| ymu.playAlbum                 | Play an album                                                                 |          ✔️ |
| ymu.playPlaylist              | Play a playlist                                                               |          ✔️ |
| ymu.playRadio                 | Play a radio                                                                  |          ✔️ |
| ymu.playTrack                 | Play a track                                                                  |          ✔️ |
| ymu.skipBack                  | Return to the previous track. This option does not work when playing a radio. |          ✔️ |
| ymu.skipCurrentTrack          | Skip the track                                                                |          ✔️ |
| ymu.displayHistory            | Display the list of all tracks played during the session                      |          ✔️ |
| ymu.likeOrDislikeCurrentTrack | Like or dislike the track                                                     |          ✔️ |
| ymu.pauseOrPlayCurrentTrack   | Resume or pause the track                                                     |          ✔️ |
| ymu.hqOn                      | Set the sound quality to High                                                 |          ✔️ |
| ymu.hqOff                     | Set the sound quality to Low                                                  |          ✔️ |
| ymu.copyCurrentTrack          | Copy the url of the current track to the clipboard                            |          ✔️ |
| ymu.setVolume                 | Set the sound volume                                                          |          ✔️ |
| ymu.shutdownServer            | Shutdown the server                                                           |          ✔️ |
| ymu.SetPollingInterval        | Set the server polling interval (default: 300ms)                              |          ✔️ |
| ymu.TogglePollingLogging      | Toggle the logging of the poll messages                                       |          ✔️ |
| ymu.regroupUI                 | Regroup the UI to make the buttons stick together                             |          ✔️ |
| ymu.showTrackInfo             | Open a new panel with the track information                                   |           ❌ |
